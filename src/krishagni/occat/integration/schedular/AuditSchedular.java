
package krishagni.occat.integration.schedular;

import java.sql.Connection;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import krishagni.occat.integration.dao.AuditDao;
import krishagni.occat.integration.dao.DBDetails;
import krishagni.occat.integration.dto.NotificationResponse;
import krishagni.occat.integration.dto.SubjectDto;
import krishagni.occat.integration.util.Utility;
import krishagni.occat.integration.vm.VelocityManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AuditSchedular implements Runnable
{

	private static Log log = LogFactory.getLog(AuditSchedular.class);
	private static String SCHEDULER_FIX_DELAY = "schedular.time.interval";
	private static String SUBJECT_VM_FILE = "SubjectTemplate.vm";

	public static void main(String[] args)
	{
		try
		{
			Utility.initializeProperties();
			ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(100);
			Long fixedDelay = Long.parseLong(Utility.propertiesMap.get(SCHEDULER_FIX_DELAY).trim());
			executor.scheduleWithFixedDelay(new AuditSchedular(), 0, fixedDelay, TimeUnit.MINUTES);
		}
		catch (Exception e)
		{
			log.error("Error running " + AuditSchedular.class.getName() + ": " + e.getMessage());
		}
	}

	@Override
	public void run()
	{
		AuditDao dao = new AuditDao();
		Connection conn = null;
		try
		{
			conn = DBDetails.getConnection();
			List<SubjectDto> subjects = dao.getSubjects(conn);
			for (SubjectDto subjectDto : subjects)
			{
				VelocityManager vm = VelocityManager.getInstance();
				String subjectXml = vm.evaluate(subjectDto, SUBJECT_VM_FILE);
				MirthNotifier client = new MirthNotifier();
				NotificationResponse notifResponse = client.sendSubjects(subjectXml);
				if (!notifResponse.getStatus().toString()
						.equals(krishagni.occat.integration.dto.NotificationResponse.Status.CONNECTION_FAIL.toString()))
				{
					dao.insertNotificationStatus(conn, notifResponse, subjectDto.getIdentifier());
				}
				log.info("Subject having id " + subjectDto.getIdentifier() + "sent to mirth application");
			}

			conn.close();
		}
		catch (Exception e)
		{
			log.error("Error running " + AuditSchedular.class.getName() + ": " + e.getMessage() + e.getCause());
		}
	}

}
