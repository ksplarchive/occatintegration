
package krishagni.occat.integration.schedular;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import krishagni.occat.integration.dto.NotificationResponse;
import krishagni.occat.integration.dto.NotificationResponse.Status;
import krishagni.occat.integration.util.Utility;

public class MirthNotifier
{

	private static String MIRTH_URL = "mirth.url";

	/**
	 * This methods send subject details vai xml file on http port
	 * @param xml
	 * @throws Exception 
	 */
	public NotificationResponse sendSubjects(String xml) throws Exception
	{
		URL url;
		HttpURLConnection connection = null;
		try
		{
			//Create connection
			url = new URL(Utility.propertiesMap.get(MIRTH_URL));
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/xml");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(xml.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");

			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			//Send request
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(xml);
			wr.flush();
			wr.close();

			//Get Response	
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = rd.readLine()) != null)
			{
				response.append(line);
			}
			rd.close();
			String status = response.toString();

			if (Status.SUCCESS.toString().equalsIgnoreCase(status))
			{
				return NotificationResponse.success("Subject Created Suceessfully");
			}
			else
			{
				return NotificationResponse.fail("Error while inserting data into Mirth database");
			}

		}
		catch (IOException e)
		{
			return NotificationResponse.connectionFail("Error while inserting data into Mirth database");
		}
		finally
		{

			if (connection != null)
			{
				connection.disconnect();
			}
		}
	}

}
