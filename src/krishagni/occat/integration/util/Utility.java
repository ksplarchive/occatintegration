
package krishagni.occat.integration.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Utility
{

	private static Log logger = LogFactory.getLog(Utility.class);
	private static String PROPERTIES_FILENAME = "../OC-caTissueIntegration.properties";
	public static Map<String, String> propertiesMap = new HashMap<String, String>();

	public static void initializeProperties() throws Exception
	{
		final Properties properties = new Properties();
		try
		{
			properties.load(new FileInputStream(PROPERTIES_FILENAME));
			for (String key : properties.stringPropertyNames())
			{
				String value = properties.getProperty(key);
				propertiesMap.put(key, value);
			}
		}
		catch (FileNotFoundException ex)
		{
			logger.error("Property file '" + PROPERTIES_FILENAME + "' not found", ex);
			throw new Exception("Property file '" + PROPERTIES_FILENAME + "' not found");
		}
		catch (IOException ex)
		{
			logger.error("Error while reading '" + PROPERTIES_FILENAME + "' file", ex);
			throw new Exception("Error while reading '" + PROPERTIES_FILENAME + "' file");
		}
	}
}
