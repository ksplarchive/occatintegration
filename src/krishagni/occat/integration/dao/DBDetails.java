
package krishagni.occat.integration.dao;

import java.sql.Connection;
import java.sql.DriverManager;

import krishagni.occat.integration.util.Utility;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class has the DB connection details.
 * 
 */
public class DBDetails
{

	private static Log logger = LogFactory.getLog(DBDetails.class);
	private static String host;
	private static String userName;
	private static String pwd;
	private static String url;
	private static String port;
	private static String dbName;

	/**
	 *
	 * @param prop
	 */
	public static void readProperties()
	{
		host = Utility.propertiesMap.get("database.host");
		userName = Utility.propertiesMap.get("database.username");
		pwd = Utility.propertiesMap.get("database.password");
		dbName = Utility.propertiesMap.get("database.name");
		port = Utility.propertiesMap.get("database.port");

		final StringBuffer buffer = new StringBuffer(20);
		buffer.append("jdbc:postgresql://");
		buffer.append(host);
		buffer.append(':');
		buffer.append(port);
		buffer.append('/');
		buffer.append(dbName);
		url = buffer.toString();

	}

	public static Connection getConnection() throws Exception
	{
		readProperties();
		Connection connection = null;
		connection = DriverManager.getConnection(url, userName, pwd);
		return connection;

	}

}
