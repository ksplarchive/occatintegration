
package krishagni.occat.integration.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import krishagni.occat.integration.dto.NotificationResponse;
import krishagni.occat.integration.dto.SubjectDto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AuditDao
{

	private static Log logger = LogFactory.getLog(AuditDao.class);
	private static final String GET_SUBJECTS = "select audit.audit_id as audit_id,sub.label as label,sub.enrollment_date as enrollment_date,subject.gender as gender,subject.date_of_birth,study.unique_identifier as unique_identifier, et.name as operation "
			+ "from audit_log_event audit "
			+ "inner join audit_log_event_type et  on audit.audit_log_event_type_id = et.audit_log_event_type_id "
			+ "inner join study_subject sub on sub.study_subject_id =  audit.entity_id "
			+ "inner join subject on  subject.subject_id = sub.subject_id "
			+ "inner join study on sub.study_id = study.study_id "
			+ "left join krishagni_notification_status kns on audit.audit_id = kns.audit_id "
			+ "where (et.name='Study subject created' or et.name='Study subject value changed')  and kns.audit_id is null";

	private static final String INSERT_NOTIFICATION_STATUS = "insert into krishagni_notification_status(audit_id,status,comments,updated_date) values(?,?,?,?)";

	public List<SubjectDto> getSubjects(Connection conn) throws Exception
	{
		List<SubjectDto> subjectDtos = new ArrayList<SubjectDto>();
		try
		{
			PreparedStatement stmt = conn.prepareStatement(GET_SUBJECTS);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				SubjectDto subject = new SubjectDto();
				subject.setIdentifier(rs.getLong("audit_id"));
				subject.setLabel(rs.getString("label"));
				subject.setEnrollmentDate(rs.getTimestamp("enrollment_date"));
				subject.setGender(rs.getString("gender"));
				subject.setStudyId(rs.getString("unique_identifier"));
				subject.setBirthDate(rs.getDate("date_of_birth"));

				if (rs.getString("operation").equals("Study subject created"))
					subject.setOperation("Add");
				else if (rs.getString("operation").equals("Study subject value changed"))
					subject.setOperation("Update");
				subjectDtos.add(subject);
			}
		}
		catch (SQLException e)
		{
			throw new Exception("Error while retriving Subjects from Database", e);
		}

		return subjectDtos;
	}

	public void insertNotificationStatus(Connection conn, NotificationResponse notifResponse, Long identifier)
			throws SQLException
	{
		PreparedStatement stmt = conn.prepareStatement(INSERT_NOTIFICATION_STATUS);
		stmt.setLong(1, identifier);
		stmt.setString(2, notifResponse.getStatus().toString());
		stmt.setString(3, notifResponse.getMessage());
		stmt.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
		stmt.executeUpdate();
	}
}
