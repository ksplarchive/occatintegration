
package krishagni.occat.integration.vm;

import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

public class VelocityManager
{

	private static VelocityManager velocityManager;
	private static VelocityEngine velocityEngine;
	private static final String OBJECT_LIST = "subjectDto";
	private static final String VELOCITY_CLASSPATH_RESOURCE_LOADER = "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader";
	private static final String CLASS_RESOURCE_LOADER_CLASS = "class.resource.loader.class";
	private static final String RESOURCE_LOADER = "resource.loader";
	private static final String CLASS = "class";

	/**
	 * Singleton class
	 * 
	 * @throws Exception
	 */
	private VelocityManager() throws Exception
	{
		initializeVelocityEngine();
	}

	/**
	 * replaces Object value in the template and gives the resultant o/p
	 * 
	 * @param objectList
	 * @param templateFileName
	 * @return
	 * @throws Exception
	 */
	public String evaluate(List<?> objectList, String templateFileName) throws Exception
	{
		Template template = velocityEngine.getTemplate(templateFileName);
		VelocityContext context = new VelocityContext();
		context.put(OBJECT_LIST, objectList);
		StringWriter writer = new StringWriter();
		template.merge(context, writer);
		return writer.toString();
	}

	public String evaluate(Object object, String templateFileName) throws Exception
	{
		Template template = velocityEngine.getTemplate(templateFileName);
		VelocityContext context = new VelocityContext();
		context.put(OBJECT_LIST, object);
		StringWriter writer = new StringWriter();
		template.merge(context, writer);
		return writer.toString();
	}

	public String evaluate(Map<String, Object> objs, String templateFileName) throws Exception
	{

		Template template = velocityEngine.getTemplate(templateFileName);
		VelocityContext context = new VelocityContext();
		if (objs != null)
		{
			for (Map.Entry<String, Object> gridObj : objs.entrySet())
			{
				context.put(gridObj.getKey(), gridObj.getValue());
			}
		}

		StringWriter writer = new StringWriter();
		template.merge(context, writer);
		return writer.toString();
	}

	public static VelocityManager getInstance() throws Exception
	{
		if (velocityManager == null)
		{
			velocityManager = new VelocityManager();
		}
		return velocityManager;
	}

	private void initializeVelocityEngine() throws Exception
	{
		velocityEngine = new VelocityEngine();
		velocityEngine.setProperty(RESOURCE_LOADER, CLASS);
		velocityEngine.setProperty(CLASS_RESOURCE_LOADER_CLASS, VELOCITY_CLASSPATH_RESOURCE_LOADER);
		velocityEngine.init();
	}
}
