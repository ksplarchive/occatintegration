
package krishagni.occat.integration.dto;

public class NotificationResponse
{

	public enum Status {
		SUCCESS, FAIL, CONNECTION_FAIL
	};

	private Status status;

	private String message;

	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public static NotificationResponse success(String message)
	{
		NotificationResponse notificationResponse = new NotificationResponse();
		notificationResponse.setMessage(message);
		notificationResponse.setStatus(Status.SUCCESS);
		return notificationResponse;
	}

	public static NotificationResponse fail(String message)
	{
		NotificationResponse notificationResponse = new NotificationResponse();
		notificationResponse.setMessage(message);
		notificationResponse.setStatus(Status.FAIL);
		return notificationResponse;
	}

	public static NotificationResponse connectionFail(String message)
	{
		NotificationResponse notificationResponse = new NotificationResponse();
		notificationResponse.setMessage(message);
		notificationResponse.setStatus(Status.CONNECTION_FAIL);
		return notificationResponse;
	}
}
