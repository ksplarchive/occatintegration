
package krishagni.occat.integration.dto;

import java.util.Date;

public class SubjectDto
{

	private Long identifier;

	private String label;

	private Date enrollmentDate;

	private String gender;

	private String studyId;

	private String operation;

	private Date birthDate;

	public Long getIdentifier()
	{
		return identifier;
	}

	public void setIdentifier(Long identifier)
	{
		this.identifier = identifier;
	}

	public String getLabel()
	{
		return label;
	}

	public void setLabel(String label)
	{
		this.label = label;
	}

	public Date getEnrollmentDate()
	{
		return enrollmentDate;
	}

	public void setEnrollmentDate(Date enrollmentDate)
	{
		this.enrollmentDate = enrollmentDate;
	}

	public String getGender()
	{
		return gender;
	}

	public void setGender(String gender)
	{
		this.gender = gender;
	}

	public String getStudyId()
	{
		return studyId;
	}

	public void setStudyId(String studyId)
	{
		this.studyId = studyId;
	}

	public String getOperation()
	{
		return operation;
	}

	public void setOperation(String operation)
	{
		this.operation = operation;
	}

	public Date getBirthDate()
	{
		return birthDate;
	}

	public void setBirthDate(Date birthDate)
	{
		this.birthDate = birthDate;
	}

}
